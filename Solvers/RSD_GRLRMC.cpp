
#include "Solvers/RSD_GRLRMC.h"

/*Define the namespace*/
namespace ROPTLIB{

	RSD_GRLRMC::RSD_GRLRMC(const Problem *prob, const Variable *initialx, const Variable *insoln)
	{
		Initialization(prob, initialx, insoln);
	};

	void RSD_GRLRMC::SetProbX(const Problem *prob, const Variable *initialx, const Variable *insoln)
	{
		SolversLS_GRLRMC::SetProbX(prob, initialx, insoln);
		prob->SetUseGrad(true);
		prob->SetUseHess(false);
	};

	void RSD_GRLRMC::SetDefaultParams()
	{
		SolversLS_GRLRMC::SetDefaultParams();
		InitSteptype = BBSTEP;
		SolverName.assign("RSD_GRLRMC");
	};

	void RSD_GRLRMC::GetSearchDir(void)
	{
		Mani->ScaleTimesVector(x1, -1, gf1, eta1);
	};
}; /*end of ROPTLIB namespace*/
