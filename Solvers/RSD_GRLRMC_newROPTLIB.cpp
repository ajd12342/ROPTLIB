
#include "Solvers/RSD_GRLRMC.h"

/*Define the namespace*/
namespace ROPTLIB{

	RSD_GRLRMC::RSD_GRLRMC(const Problem *prob, const Variable *initialx, const Variable *insoln)
	{
		Initialization(prob, initialx, insoln);
	};

	void RSD_GRLRMC::SetProbX(const Problem *prob, const Variable *initialx, const Variable *insoln)
	{
		SolversLS_GRLRMC::SetProbX(prob, initialx, insoln);
		prob->SetUseGrad(true);
		prob->SetUseHess(false);
	};

	void RSD_GRLRMC::SetDefaultParams()
	{
		SolversLS_GRLRMC::SetDefaultParams();
		InitSteptype = BBSTEP;
		SolverName.assign("RSD_GRLRMC");
	};

	void RSD_GRLRMC::GetSearchDir(void)
	{
		Prob->PreConditioner(x1, gf1, eta1);
		Mani->ScaleTimesVector(x1, -1, eta1, eta1);
		
		if (Mani->Metric(x1, eta1, gf1) / ngf1 / ngf1 >= -std::sqrt(std::numeric_limits<double>::epsilon()))
		{
			printf("Warning:Preconditioner gives an non-decreasing direction!Reset the search direction to the negative gradient\n");
			Mani->ScaleTimesVector(x1, -1, gf1, eta1);
		}
	};
}; /*end of ROPTLIB namespace*/
