/*
This file defines the class of the Riemannian steepest descent method. See [AMS2008]
	[AMS2008]: P.-A. Absil, R. Mahony, and R. Sepulchre. Optimization algorithms on matrix manifolds.
	Princeton University Press, Princeton, NJ, 2008.

Solvers --> QuasiNewton --> SolversLS_GRLRMC --> RSD_GRLRMC
---- WH
Required because SolversLS_GRLRMC is required for GRLRMC problem.

*/

#ifndef RSD_GRLRMC_H
#define RSD_GRLRMC_H

#include "Solvers/SolversLS_GRLRMC.h"
#include "Others/def.h"

/*Define the namespace*/
namespace ROPTLIB{

	class RSD_GRLRMC : public SolversLS_GRLRMC{
	public:
		/*The contructor of RSD_GRLRMC method. It calls the function Solvers::Initialization.
		INPUT : prob is the problem which defines the cost function, gradient and possible the action of Hessian
		and specifies the manifold of domain.
		initialx is the initial iterate.
		insoln is the true solution. It is not required and only used for research.*/
		RSD_GRLRMC(const Problem *prob, const Variable *initialx, const Variable *insoln = nullptr);

		/*Call Solvers::SetProbX function and indicate RCG does not need action of Hessian.
		INPUT:	prob is the problem which defines the cost function, gradient and possible the action of Hessian
		and specifies the manifold of domain.
		initialx is the initial iterate.
		insoln is the true solution. It is not required and only used for research.*/
		virtual void SetProbX(const Problem *prob, const Variable *initialx, const Variable *insoln);

		/*Setting parameters (member variables) to be default values */
		virtual void SetDefaultParams();
	protected:
		/*Set the search direction to be negative gradient*/
		virtual void GetSearchDir();
	};
}; /*end of ROPTLIB namespace*/
#endif // end of RSD_GRLRMC_H
