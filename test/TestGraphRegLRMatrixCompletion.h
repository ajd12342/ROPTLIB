/* 
This file manages input from MATLAB
and tests the GraphRegLRMatrixCompletion.

---- Diwan Anuj Jitendra, 2019
*/
#ifndef TESTGRAPHREGLRMATRIXCOMPLETION_H
#define TESTGRAPHREGLRMATRIXCOMPLETION_H

#include "Others/def.h"
#include "Others/randgen.h"
#include "Manifolds/Manifold.h"
#include "Problems/Problem.h"
#include "Solvers/SolversLS_GRLRMC.h"
#include "Solvers/RSD_GRLRMC.h"
#include "Solvers/RCG_GRLRMC.h"

#include "Problems/GraphRegLRMatrixCompletion/GraphRegLRMatrixCompletion.h"
#include "Manifolds/Euclidean/Euclidean.h"
#include "Manifolds/Euclidean/EucVariable.h"
#include "Manifolds/ProductElement.h"
#include "Manifolds/TwoFactorManifold/TwoFactorManifold.h"
#include "Manifolds/TwoFactorManifold/TwoFactorVariable.h"
#include "Manifolds/Element.h"

/*If this test file is called from Matlab, then functions in DriverMexProb.h are used.*/
// #include "test/DriverMexProb.h"
#include <algorithm>
#include <chrono>
#include <ctime>
#include <iostream>
#include <random>
#include <vector>
#include <cstring>
#include <utility>
#include <fenv.h>
using namespace ROPTLIB;

#endif