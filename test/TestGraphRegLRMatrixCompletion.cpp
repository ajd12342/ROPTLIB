#include "test/TestGraphRegLRMatrixCompletion.h"

using namespace ROPTLIB;

/*If it is compiled in Matlab, then the following "mexFunction" is used as the entrance.*/

/*This function checks the number and formats of input parameters.
nlhs: the number of output in mxArray format
plhs: the output objects in mxArray format
nrhs: the number of input in mxArray format
prhs: the input objects in mxArray format */

/*
Inputs:
data : m*n sparse matrix
The dimensions of this matrix will be stored as m and n.
data_val : m*n sparse matrix
r : long int
Lr : m*m sparse matrix 
Lc : n*n sparse matrix
NOTE: dimensions will be checked
alpha_r, alpha_c, gamma_r, gamma_c : double
G, H : initial iterates as m*r and n*r matrices
optimizer : "RSD" or "RCG"
metric : 0 for preconditioned, 1 for left-invariant
Outputs:
Gfinal, Hfinal : Final iterates
stats: statistics in a format
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// Trigger a SIGFPE on NaN, inf and division by zero
	// feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	//Check number of arguments
	if(nrhs!=13){
		mexErrMsgTxt("The number of rhs arguments must be 13.\n");
	}
	if(nlhs!=3){
		mexErrMsgTxt("The number of lhs arguments must be 3.\n");
	}
	// Input data, matrix dimensions, input mask
	const mxArray* dataArr = prhs[0];
	integer m=mxGetM(dataArr);
	integer n=mxGetN(dataArr);
	// std::cout<<"m= "<<m<<",n= "<<n<<std::endl;
	
	mwIndex* jcinterm=mxGetJc(dataArr);
	integer* nonzerosj = new integer[n];
	for(integer i=0;i<n;i++){
		nonzerosj[i]=jcinterm[i+1]-jcinterm[i];
	}

	integer nz = jcinterm[n];
	// std::cout<<"nz= "<<nz<<std::endl;

	double* data = mxGetPr(dataArr);
	mwIndex* irinterm=mxGetIr(dataArr);
	integer* ir = new integer[nz];
	for(integer i=0;i<nz;i++){
		ir[i]=irinterm[i];
	}
	
	integer* jc = new integer[nz];
	integer iteratordata=0;
	for(integer i=0;i<n;i++){
		for(integer j=0;j<nonzerosj[i];j++,iteratordata++){
			jc[iteratordata]=i;
		}
	}
	// std::cout<<"input info"<<std::endl;
	// for(integer i=0;i<nz;i++){
	// 	std::cout<<data[i]<<' '<<ir[i]<<' '<<jc[i]<<std::endl;
	// }

	// Input validation data, matrix dimensions, input mask
	const mxArray* dataValArr = prhs[1];
	mwIndex* jcintermVal=mxGetJc(dataValArr);
	integer* nonzerosjVal = new integer[n];
	for(integer i=0;i<n;i++){
		nonzerosjVal[i]=jcintermVal[i+1]-jcintermVal[i];
	}

	integer nzVal = jcintermVal[n];
	// std::cout<<"nz= "<<nz<<std::endl;

	double* dataVal = mxGetPr(dataValArr);
	mwIndex* irintermVal=mxGetIr(dataValArr);
	integer* irVal = new integer[nzVal];
	for(integer i=0;i<nzVal;i++){
		irVal[i]=irintermVal[i];
	}
	
	integer* jcVal = new integer[nzVal];
	integer iteratordataVal=0;
	for(integer i=0;i<n;i++){
		for(integer j=0;j<nonzerosjVal[i];j++,iteratordataVal++){
			jcVal[iteratordataVal]=i;
		}
	}
	// std::cout<<"input info"<<std::endl;
	// for(integer i=0;i<nz;i++){
	// 	std::cout<<dataVal[i]<<' '<<ir[i]<<' '<<jc[i]<<std::endl;
	// }

	// Get the rank
	integer r=(integer)round(*mxGetPr(prhs[2]));

	// std::cout<<"r= "<<r<<std::endl;

	// Get the Laplacians, as sparse matrices
	const mxArray* LrArr = prhs[3];
	const mxArray* LcArr = prhs[4];

	//Check dimensions
	if(!(mxGetM(LrArr)==m && mxGetN(LrArr)==m && mxGetM(LcArr)==n && mxGetN(LcArr)==n)){
		mexErrMsgTxt("Incorrect dimensions for Laplacians.\n");
	}
	mwIndex* jcLr = mxGetJc(LrArr);
	integer* nonzerosjLr = new integer[m];
	for(integer i=0;i<m;i++){
		nonzerosjLr[i]=jcLr[i+1]-jcLr[i];
	}
	integer nr=jcLr[m];
	double* Lr = mxGetPr(LrArr);
	mwIndex* iLrinterm = mxGetIr(LrArr);
	integer* iLr = new integer[nr];
	for(integer i=0;i<nr;i++){
		iLr[i]=iLrinterm[i];
	}
	// std::cout<<"Here"<<std::endl;

	integer* jLr = new integer[nr];
	integer iteratorr=0;
	for(integer i=0;i<m;i++){
		for(integer j=0;j<nonzerosjLr[i];j++,iteratorr++){
			jLr[iteratorr]=i;
		}
	}

	// std::cout<<"nr= "<<nr<<",x= "<<mxGetM(LrArr)<<",y= "<<mxGetN(LrArr)<<std::endl;
	// std::cout<<"Lr= "<<std::endl;
	// for(integer i=0;i<nr;i++){
	// 	std::cout<<'('<<iLr[i]<<','<<jLr[i]<<") "<<Lr[i]<<std::endl;
	// }
	mwIndex* jcLc = mxGetJc(LcArr);
	integer* nonzerosjLc = new integer[n];
	for(integer i=0;i<n;i++){
		nonzerosjLc[i]=jcLc[i+1]-jcLc[i];
	}
	integer nc=jcLc[n];
	double* Lc = mxGetPr(LcArr);
	// std::cout<<"nc= "<<nc<<std::endl;
	mwIndex* iLcinterm = mxGetIr(LcArr);
	// std::cout<<"Here3"<<std::endl;
	integer* iLc = new integer[nc];
	for(integer i=0;i<nc;i++){
		iLc[i]=iLcinterm[i];
	}
	// std::cout<<"Here4"<<std::endl;
	integer* jLc = new integer[nc];
	integer iteratorc=0;
	for(integer i=0;i<n;i++){
		for(integer j=0;j<nonzerosjLc[i];j++,iteratorc++){
			jLc[iteratorc]=i;
		}
	}

	// std::cout<<"nc= "<<nc<<",x= "<<mxGetM(LcArr)<<",y= "<<mxGetN(LcArr)<<std::endl;
	// std::cout<<"Lc= "<<std::endl;
	// for(integer i=0;i<nc;i++){
	// 	std::cout<<'('<<iLc[i]<<','<<jLc[i]<<") "<<Lc[i]<<std::endl;
	// }

	// Other hyperparameters
	double alpha_r = *mxGetPr(prhs[5]);
	double alpha_c = *mxGetPr(prhs[6]);
	double gamma_r = *mxGetPr(prhs[7]);
	double gamma_c = *mxGetPr(prhs[8]);

	// std::cout<<"alpha_r= "<<alpha_r<<",alpha_c= "<<alpha_c<<",gamma_r= "<<gamma_r<<",gamma_c= "<<gamma_c<<std::endl;
	// The domain manifold

	integer metricChoice = (integer)round(*mxGetPr(prhs[12]));
	if(!(metricChoice==0 || metricChoice==1)){
		mexErrMsgTxt("Metric must either be 0 or 1.\n");
	}
	TwoFactorManifold domain(m,n,r,metricChoice);

	// Make the object
	GraphRegLRMatrixCompletion grmc(
		data,nz,ir,jc,
		dataVal,nzVal,irVal,jcVal,
		m,n,r,
		Lr,nr,iLr,jLr,
		Lc,nc,iLc,jLc,
		alpha_r,alpha_c,gamma_r,gamma_c);
	grmc.SetDomain(&domain);

	// Make the initial iterate G,H
	TwoFactorVariable GH(m,n,r);
	GH.NewMemoryOnWrite();

	double* Gptr=GH.GetElement(0)->ObtainWriteEntireData();
	double* Hptr=GH.GetElement(1)->ObtainWriteEntireData();
	if(!(mxGetM(prhs[9])==m && mxGetN(prhs[9])==r && mxGetM(prhs[10])==n && mxGetN(prhs[10])==r)){
		mexErrMsgTxt("Dimensions of G and/or H are incorrect.\n");
	}
	mxDouble* Ginp=mxGetPr(prhs[9]);
	mxDouble* Hinp=mxGetPr(prhs[10]);
	for(integer i=0;i<(m*r);i++){
		Gptr[i]=Ginp[i];
	}
	for(integer i=0;i<(n*r);i++){
		Hptr[i]=Hinp[i];
	}

	// GH.Print("GH",false);
	// grmc.CheckGradHessian(&GH);
	// Perform optimization

	// Get length of the optimizer text

	integer optimLen = mxGetN(prhs[11]);
	char optimStr[optimLen];

	// Get the string

	mxGetString(prhs[11],optimStr,optimLen+1);

	PARAMSMAP params;
	/* 
	0 : No output
	1 : Final iterate
	2 : Every iterate
	3 : Detailed
	*/
	params["DEBUG"]=1;
	TwoFactorVariable GH_final(m,n,r);
	if(strcmp(optimStr,"RSD")==0){
		// Make the solver

		RSD_GRLRMC RSDSolver(&grmc,&GH);

		// Set custom parameters
		RSDSolver.SetParams(params);

		// Output the parameters
		RSDSolver.CheckParams();
		RSDSolver.Run();

		// Get final iterate 
		TwoFactorVariable* GH_final_temp = (TwoFactorVariable*)(RSDSolver.GetXopt());
		double* timeSeries_temp = RSDSolver.Gettime2Series();
		double* trainRMSESeries_temp = RSDSolver.GettrainRMSESeries();
		double* testRMSESeries_temp = RSDSolver.GettestRMSESeries();
		integer maxiter = RSDSolver.GetIter();

		plhs[2] = mxCreateDoubleMatrix(maxiter+1,3,mxREAL);		
		double* stats = mxGetPr(plhs[2]);

		GH_final_temp->CopyTo(&GH_final);
		for(int i=0;i<maxiter+1;i++){
			stats[i]=timeSeries_temp[i];
		}
		for(int i=0;i<maxiter+1;i++){
			stats[maxiter+1+i]=trainRMSESeries_temp[i];
		}

		for(int i=0;i<maxiter+1;i++){
			stats[2*(maxiter+1)+i]=testRMSESeries_temp[i];
		}

	}else if(strcmp(optimStr,"RCG")==0){
		// Make the solver
		RCG_GRLRMC RCGSolver(&grmc,&GH);

		// Set custom parameters
		RCGSolver.SetParams(params);

		// Output the parameters
		RCGSolver.CheckParams();
		RCGSolver.Run();

		// Get final iterate
		TwoFactorVariable* GH_final_temp = (TwoFactorVariable*)(RCGSolver.GetXopt());
		double* timeSeries_temp = RCGSolver.Gettime2Series();
		double* trainRMSESeries_temp = RCGSolver.GettrainRMSESeries();
		double* testRMSESeries_temp = RCGSolver.GettestRMSESeries();
		integer maxiter = RCGSolver.GetIter();

		plhs[2] = mxCreateDoubleMatrix(maxiter+1,3,mxREAL);		
		double* stats = mxGetPr(plhs[2]);

		GH_final_temp->CopyTo(&GH_final);
		for(int i=0;i<maxiter+1;i++){
			stats[i]=timeSeries_temp[i];
		}
		for(int i=0;i<maxiter+1;i++){
			stats[maxiter+1+i]=trainRMSESeries_temp[i];
		}

		for(int i=0;i<maxiter+1;i++){
			stats[2*(maxiter+1)+i]=testRMSESeries_temp[i];
		}

	}else{
		mexErrMsgTxt("Optimizer must be either 'RSD' or 'RCG'\n");
	}
	const double* G_finalptr=GH_final.GetElement(0)->ObtainReadData();
	plhs[0] = mxCreateDoubleMatrix(r,m,mxREAL);
	double* G_finalptrplhs = mxGetPr(plhs[0]);
	for(integer i=0;i<(m*r);i++){
		G_finalptrplhs[i]=G_finalptr[i];
	}

	const double* H_finalptr=GH_final.GetElement(1)->ObtainReadData();
	plhs[1] = mxCreateDoubleMatrix(r,n,mxREAL);
	double* H_finalptrplhs = mxGetPr(plhs[1]);
	for(integer i=0;i<(n*r);i++){
		H_finalptrplhs[i]=H_finalptr[i];
	}

	delete[] ir;
	delete[] nonzerosj;
	delete[] jc;

	delete[] irVal;
	delete[] nonzerosjVal;
	delete[] jcVal;
	
	delete[] iLr;
	delete[] nonzerosjLr;
	delete[] jLr;

	delete[] iLc;
	delete[] nonzerosjLc;
	delete[] jLc;
	return;	
}