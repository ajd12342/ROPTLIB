
#include "Manifolds/TwoFactorManifold/TwoFactorVariable.h"

/*Define the namespace*/
namespace ROPTLIB{

	TwoFactorVariable::TwoFactorVariable(integer m, integer n, integer r){
		EucVariable G(m, r);
		EucVariable H(n, r);

		Element **Elems = new Element *[2];
		Elems[0] = &G;
		Elems[1] = &H;
		integer *powsintev = new integer[3];
		powsintev[0] = 0;
		powsintev[1] = 1;
		powsintev[2] = 2;

		ProductElementInitialization(Elems, 2, powsintev, 2);

		delete[] powsintev;
		delete[] Elems;
	};

	TwoFactorVariable::TwoFactorVariable(Element* G, Element* H){
		Element **Elems = new Element *[2];
		Elems[0] = G;
		Elems[1] = H;
		integer *powsintev = new integer[3];
		powsintev[0] = 0;
		powsintev[1] = 1;
		powsintev[2] = 2;

		ProductElementInitialization(Elems, 2, powsintev, 2);

		delete[] powsintev;
		delete[] Elems;
	}

	TwoFactorVariable::~TwoFactorVariable(void){
		// // Deletes the blas_sparse_matrix
		// if(TempDataExist("SSparse")){
		// 	const blas_sparse_matrix S=*(ObtainReadTempData("SSparse")->ObtainReadData());
		// 	BLAS_usds(S);
		// }
	};

	TwoFactorVariable *TwoFactorVariable::ConstructEmpty(void) const
	{
		integer m = elements[0]->Getsize()[0];
		integer r = elements[0]->Getsize()[1];
		integer n = elements[1]->Getsize()[0];
		return new TwoFactorVariable(m, n, r);
	};
}; /*end of ROPTLIB namespace*/
