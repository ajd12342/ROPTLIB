/*
This file defines the class of a point on the tangent space of low-rank manifold R_r^{m times n}
using the two-factor representation.

SmartSpace --> ProductElement --> TwoFactorVector

---- Diwan Anuj Jitendra, 2019
*/

#ifndef TWOFACTORVECTOR_H
#define TWOFACTORVECTOR_H

#include "Manifolds/ProductElement.h"
#include "Manifolds/Euclidean/EucVector.h"

/*Define the namespace*/
namespace ROPTLIB{

	class TwoFactorVector : public ProductElement{
	public:
		/*Construct an empty vector on the R_r^{m \times n} with only size information.
		The representation of the tangent vector is
		R^{m times r} times R^{n times r}^T */
		TwoFactorVector(integer m, integer n, integer r);

		/* Construct it using ready-to-use Elements. No size checks are performed, yet. */
		TwoFactorVector(Element* dG, Element* dH);

		/*Destruct by deleting variables*/
		virtual ~TwoFactorVector(void);

		/*Create an object of TwoFactorVector with same size as this TwoFactorVector.*/
		virtual TwoFactorVector *ConstructEmpty(void) const;
	};
}; /*end of ROPTLIB namespace*/
#endif // end of TWOFACTORVECTOR_H
