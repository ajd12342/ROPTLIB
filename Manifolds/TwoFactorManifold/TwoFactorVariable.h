/*
This file defines the class of a point on the low-rank manifold R_r^{m times n}, which is represented by 
R^{m \times r} \times R^{n \times r}^T.

SmartSpace --> ProductElement --> TwoFactorVariable

---- WH
*/

#ifndef TWOFACTORVARIABLE_H
#define TWOFACTORVARIABLE_H

#include "Manifolds/ProductElement.h"
#include "Manifolds/Euclidean/EucVariable.h"
#include "Others/SparseBLAS/blas_sparse.h"

/*Define the namespace*/
namespace ROPTLIB{

	class TwoFactorVariable : public ProductElement{
	public:
		/*Construct an empty variable on the low-rank manifold R_r^{m times n} with only size information.*/
		TwoFactorVariable(integer m, integer n, integer r);

		/* Construct it using ready-to-use Elements. No size checks are performed, yet. */
		TwoFactorVariable(Element* G, Element* H);

		/*Destruct by deleting all variables*/
		virtual ~TwoFactorVariable(void);

		/*Create an object of TwoFactorVariable with same size as this TwoFactorVariable.*/
		virtual TwoFactorVariable *ConstructEmpty(void) const;
	};
}; /*end of ROPTLIB namespace*/
#endif // end of TWOFACTORVARIABLE_H
