
#include "Manifolds/TwoFactorManifold/TwoFactorVector.h"

/*Define the namespace*/
namespace ROPTLIB{

	TwoFactorVector::TwoFactorVector(integer m, integer n, integer r)
	{
		EucVector dG(m, r);
		EucVector dH(n, r);

		Element **Elems = new Element *[2];
		Elems[0] = &dG;
		Elems[1] = &dH;
		integer *powsintev = new integer[3];
		powsintev[0] = 0;
		powsintev[1] = 1;
		powsintev[2] = 2;

		ProductElementInitialization(Elems, 2, powsintev, 2);

		delete[] powsintev;
		delete[] Elems;
	};

	TwoFactorVector::TwoFactorVector(Element* dG, Element* dH){
		Element **Elems = new Element *[2];
		Elems[0] = dG;
		Elems[1] = dH;
		integer *powsintev = new integer[3];
		powsintev[0] = 0;
		powsintev[1] = 1;
		powsintev[2] = 2;

		ProductElementInitialization(Elems, 2, powsintev, 2);

		delete[] powsintev;
		delete[] Elems;
	}
	
	TwoFactorVector::~TwoFactorVector(void)
	{
	};

	TwoFactorVector *TwoFactorVector::ConstructEmpty(void) const
	{
		integer m = elements[0]->Getsize()[0];
		integer r = elements[0]->Getsize()[1];
		integer n = elements[1]->Getsize()[0];
		return new TwoFactorVector(m,n,r);
	};
}; /*end of ROPTLIB namespace*/
