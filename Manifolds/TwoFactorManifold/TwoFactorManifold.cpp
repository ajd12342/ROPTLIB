#include "Manifolds/TwoFactorManifold/TwoFactorManifold.h"

/*Define the namespace*/
namespace ROPTLIB{
	TwoFactorManifold::TwoFactorManifold(integer inm, integer inn, integer inr, integer inmetric)
	: ProductManifold(2, new Euclidean(inm,inr), static_cast<integer> (1), 
						 new Euclidean(inn,inr), static_cast<integer> (1)){
		m=inm;
		n=inn;
		r=inr;
		mtimesn=m*n;
		mtimesr=m*r;
		ntimesr=n*r;
		rtimesr=r*r;
		if(inmetric==0 || inmetric==1 ){
			metric=inmetric;
		}else{
			throw std::invalid_argument("Metric should be either 0(preconditioned) or 1(left-invariant)\n");
		}
		name.assign("TwoFactorManifold");
		SetIsIntrApproach(false);
		SetHasHHR(false);
		// Need to make TwoFactorVectors instead of ProductElements
		delete EMPTYEXTR;
		delete EMPTYINTR;

		/* Do exactly the same thing as in the ProductManifold constructor
		   But replace ProductElement with TwoFactorVector
		*/

		Element **elements = new Element *[numoftotalmani];
		for (integer i = 0; i < numofmani; i++)
		{
			if (manifolds[i]->GetIsIntrinsic())
			{
				for (integer j = powsinterval[i]; j < powsinterval[i + 1]; j++)
				{
					elements[j] = const_cast<Element *> (manifolds[i]->GetEMPTYINTR());
				}
			}
			else
			{
				for (integer j = powsinterval[i]; j < powsinterval[i + 1]; j++)
				{
					elements[j] = const_cast<Element *> (manifolds[i]->GetEMPTYEXTR());
				}
			}
		}
		// EMPTYINTR specify the tangent vector using the approach given by the IsIntrApproach parameters of each manfold.
		EMPTYINTR = new TwoFactorVector(elements[0], elements[1]);

		for (integer i = 0; i < numofmani; i++)
		{
			for (integer j = powsinterval[i]; j < powsinterval[i + 1]; j++)
			{
				elements[j] = const_cast<Element *> (manifolds[i]->GetEMPTYEXTR());
			}
		}
		// EMPTYEXTR use extrinsic approach for all the manifolds
		EMPTYEXTR = new TwoFactorVector(elements[0], elements[1]);
		delete[] elements;
	}

	TwoFactorManifold::~TwoFactorManifold(void){
		for (integer i = 0; i < numofmani; i++)
		{
			delete manifolds[i];
		}
		//Why is it defined here? Won't ProductManifold destructor do its job?
	}
	double TwoFactorManifold::Metric(Variable *x, Vector *etax, Vector *xix) const{

		// Convert to TwoFactor objects and get pointers
		/* Dynamic cast is required to downcast the Variable* to TwoFactorVariable*.
		   Downcasting statically is impossible */
		TwoFactorVector* twoetax = dynamic_cast<TwoFactorVector* >(etax);
		const double* etaxGptr = twoetax->GetElement(0)->ObtainReadData();
		const double* etaxHptr = twoetax->GetElement(1)->ObtainReadData();
		
		TwoFactorVector* twoxix = dynamic_cast<TwoFactorVector* >(xix);
		const double* xixGptr = twoxix->GetElement(0)->ObtainReadData();
		const double* xixHptr = twoxix->GetElement(1)->ObtainReadData();

		// Compute xixG^T*etaxG and xixH^T*etaxH here since they are required in both metrics

		// Compute xixG^T*etaxG
		SharedSpace* xixGTetaxG= new SharedSpace(2,r,r);
		double* xixGTetaxGptr = xixGTetaxG->ObtainWriteEntireData();
		dgemm_(GLOBAL::T,GLOBAL::N,&r,&r,&m,&GLOBAL::DONE,
			xixGptr,&m,
			etaxGptr,&m,
			&GLOBAL::DZERO,
			xixGTetaxGptr,&r);

		// Compute xixH^T*etaxH
		SharedSpace* xixHTetaxH= new SharedSpace(2,r,r);
		double* xixHTetaxHptr = xixHTetaxH->ObtainWriteEntireData();
		dgemm_(GLOBAL::T,GLOBAL::N,&r,&r,&n,&GLOBAL::DONE,
			xixHptr,&n,
			etaxHptr,&n,
			&GLOBAL::DZERO,
			xixHTetaxHptr,&r);

		// Initialize the final result
		double result = 0;

		/*  
		Since the inverses are in upper storage mode, ddot_ cannot be used directly
		Since C++ for loops are nearly as fast as simple operations like ddot_,
		we use a C++ for loop here to compute the trace fast using (r^2+r)/2 operations 
		*/

		// Use the same loop for both

		// Iterate (i,j) over the upper right triangle(including the diagonal)
		
		/* 
		Use idx1,idx2 to store the 1D representations of the 2D indices (i,j) and (j,i)
		to be used to multiply with (i,j) entry of the symmetric matrix
		(i,j) -> i + j*m for column-major representation of an (m,n) matrix
		Here m=n=r
		*/

		if(metric==0){
			// Preconditioned

			//Precompute xGTG(xG^T * xG) and xHTH(xH^T * xH)
			TwoFactorManifold::GTGHTH(x);
			const double* xHTHptr = x->ObtainReadTempData("HTH")->ObtainReadData();
			const double* xGTGptr = x->ObtainReadTempData("GTG")->ObtainReadData();

			// Compute full dot of (xGTG)=(xGTG)^T and xixHTetaxH
			// Compute full dot of (xHTH)=(xHTH)^T and xixGTetaxG
			
			int idx1;
			int idx2; 
			for(integer i=0;i<r;i++){
				// Diagonal case is special since one must avoid double-counting
				// j=i
				idx1=i+i*r;
				idx2=i+i*r;
				result+= xHTHptr[idx1]*xixGTetaxGptr[idx1];
				result+= xGTGptr[idx1]*xixHTetaxHptr[idx1];

				// other j's 
				for(integer j=(i+1);j<r;j++){
					idx1=i+j*r;
					idx2=j+i*r;
					result+= xHTHptr[idx1]*(xixGTetaxGptr[idx1]+xixGTetaxGptr[idx2]);
					result+= xGTGptr[idx1]*(xixHTetaxHptr[idx1]+xixHTetaxHptr[idx2]);
				}
			}

		}else if(metric==1){
			// Left-invariant

			// Precompute xGTGinv((xG^T * xG)^(-1)) and xHTHinv((xH^T * xH)^(-1))
			TwoFactorManifold::GTGinvHTHinv(x);
			const double* xHTHinvptr = x->ObtainReadTempData("HTHinv")->ObtainReadData();
			const double* xGTGinvptr = x->ObtainReadTempData("GTGinv")->ObtainReadData();

			// Compute full dot of (xGTG)^(-1)=(xGTG)^(-T) and xixGTetaxG
			// Compute full dot of (xHTH)^(-1)=(xHTH)^(-T) and xixHTetaxH

			int idx1;
			int idx2; 
			for(integer i=0;i<r;i++){
				// Diagonal case is special since one must avoid double-counting
				// j=i
				idx1=i+i*r;
				idx2=i+i*r;
				result+= xGTGinvptr[idx1]*xixGTetaxGptr[idx1];
				result+= xHTHinvptr[idx1]*xixHTetaxHptr[idx1];

				// other j's 
				for(integer j=(i+1);j<r;j++){
					idx1=i+j*r;
					idx2=j+i*r;
					result+= xGTGinvptr[idx1]*(xixGTetaxGptr[idx1]+xixGTetaxGptr[idx2]);
					result+= xHTHinvptr[idx1]*(xixHTetaxHptr[idx1]+xixHTetaxHptr[idx2]);
				}
			}

		}else{
			// Strings work like this
			throw std::invalid_argument("Unexpected value for metric(not 0 or 1) in TwoFactorManifold,"
				" exception thrown.\n");
		}

		// Delete since they were locally created
		delete xixGTetaxG;
		delete xixHTetaxH;

		return result;
	}

	void TwoFactorManifold::EucGradToGrad(Variable *x, Vector *egf, Vector *gf, const Problem *prob) const{

		// Convert to TwoFactor objects and get pointers
		/* Dynamic cast is required to downcast the Variable* to TwoFactorVariable*.
		   Downcasting statically is impossible */
		TwoFactorVector* twoegf = dynamic_cast<TwoFactorVector* >(egf);
		TwoFactorVector* twogf = dynamic_cast<TwoFactorVector* >(gf);

		/* egf should point to the input; gf to the output.
		   If egf==gf, allowed by ROPTLIB to overwrite egf by gf,
		   Replace gf with a new space of the same size. Later copy it back into egf.
		*/

		// Should gf be copied into egf later?
		bool copyLater = false;

		// Check if egf == gf i.e. twoegf == twogf; 
		if (egf == gf)
		{
			// Create a new space of the same size
			twogf = twogf->ConstructEmpty();
			// Copy it into egf later
			copyLater=true;
		}

		// Get pointers to the contents
		const double* egfGptr = twoegf->GetElement(0)->ObtainReadData();
		const double* egfHptr = twoegf->GetElement(1)->ObtainReadData();

		/* 
		Assign new memory to ensure contiguity. Very important!
		Always do this to the ProductElement before an ObtainWriteEntireData()
		*/
		twogf->NewMemoryOnWrite();
		double* gfGptr = twogf->GetElement(0)->ObtainWriteEntireData();
		double* gfHptr = twogf->GetElement(1)->ObtainWriteEntireData();

		if(metric==0){
			// Preconditioned

			//Precompute xGTGinv((xG^T * xG)^(-1)) and xHTHinv((xH^T * xH)^(-1))
			TwoFactorManifold::GTGinvHTHinv(x);
			const double* xHTHinvptr = x->ObtainReadTempData("HTHinv")->ObtainReadData();
			const double* xGTGinvptr = x->ObtainReadTempData("GTGinv")->ObtainReadData();

			// Now multiply dG with the obtained inverse (xHTH)^(-1), which is symmetric and put into gfG
			dsymm_(GLOBAL::R,GLOBAL::U,&m,&r,&GLOBAL::DONE,
				   xHTHinvptr,&r,egfGptr,&m,&GLOBAL::DZERO,
				   gfGptr,&m);

			// Now multiply dH with the obtained inverse (xGTG)^(-1), which is symmetric and put into gfH
			dsymm_(GLOBAL::R,GLOBAL::U,&n,&r,&GLOBAL::DONE,
				   xGTGinvptr,&r,egfHptr,&n,&GLOBAL::DZERO,
				   gfHptr,&n);

		}else if(metric==1){
			// Left-invariant

			//Precompute xGTG(xG^T * xG) and xHTH(xH^T * xH)
			TwoFactorManifold::GTGHTH(x);
			const double* xGTGptr = x->ObtainReadTempData("GTG")->ObtainReadData();
			const double* xHTHptr = x->ObtainReadTempData("HTH")->ObtainReadData();

			// Multiply dG with xGTG.
			dsymm_(GLOBAL::R,GLOBAL::U,&m,&r,&GLOBAL::DONE,
				   xGTGptr,&r,egfGptr,&m,&GLOBAL::DZERO,
				   gfGptr,&m);

			// Multiply dH with xHTH.
			dsymm_(GLOBAL::R,GLOBAL::U,&n,&r,&GLOBAL::DONE,
				   xHTHptr,&r,egfHptr,&n,&GLOBAL::DZERO,
				   gfHptr,&n);
			
		}else{
			// Strings work like this
			throw std::invalid_argument("Unexpected value for metric(not 0 or 1) in TwoFactorManifold,"
				" exception thrown.\n");
		}

		if(copyLater){
			twogf->CopyTo(twoegf);
			delete twogf;
		}
	}

	void TwoFactorManifold::GTGHTH(Variable *x) const{

		// Check for existence of GTG and HTH 
		if(x->TempDataExist("GTG") && x->TempDataExist("HTH")){
			return;
		}else{
			// Convert to TwoFactor objects and get pointers
			TwoFactorVariable* twox = dynamic_cast<TwoFactorVariable* >(x);
			const double* xGptr = twox->GetElement(0)->ObtainReadData();
			const double* xHptr = twox->GetElement(1)->ObtainReadData();

			// Compute xH^T * xH
			SharedSpace* xHTH = new SharedSpace(2,r,r);
			double* xHTHptr = xHTH->ObtainWriteEntireData();
			
			// Use dsyrk to compute HTH since it is faster
			dsyrk_(GLOBAL::U,GLOBAL::T,&r,&n,&GLOBAL::DONE,
				xHptr,&n, &GLOBAL::DZERO, xHTHptr,&r);

			// Compute xG^T * xG
			SharedSpace* xGTG = new SharedSpace(2,r,r);
			double* xGTGptr = xGTG->ObtainWriteEntireData();
			
			// Use dsyrk to compute GTG since it is faster
			dsyrk_(GLOBAL::U,GLOBAL::T,&r,&m,&GLOBAL::DONE,
				xGptr,&m, &GLOBAL::DZERO, xGTGptr,&r);

			// Add them to x
			x->AddToTempData("GTG",xGTG);
			x->AddToTempData("HTH",xHTH);
			return;
		}
	}

	void TwoFactorManifold::GTGinvHTHinv(Variable *x) const{
		// Check for existence of GTGinv and HTHinv
		if(x->TempDataExist("GTGinv") && x->TempDataExist("HTHinv")){
			return;
		}else{
			//Precompute xGTG(xG^T * xG) and xHTH(xH^T * xH)
			TwoFactorManifold::GTGHTH(x);
			const SharedSpace* xGTG = x->ObtainReadTempData("GTG");
			const SharedSpace* xHTH = x->ObtainReadTempData("HTH");
			
			//Integer to store info about results of operations
			integer info;

			// Compute (xGTG)^(-1)

			// Construct an object for (xGTG)^(-1) initially holding xGTG since dpotrf_ uses in/out argument
			SharedSpace* xGTGinv = xGTG->ConstructEmpty();
			xGTG->CopyTo(xGTGinv);

			// Use ObtainWritePartialData() to preserve the previous data but allow modification
			double* xGTGinvptr = xGTGinv->ObtainWritePartialData();
			
			// Get the U of Cholesky decomposition of xGTG
			dpotrf_(GLOBAL::U,&r,xGTGinvptr,&r,&info);
			
			// Check success
			if(info!=0){
				throw std::runtime_error("Cholesky decomposition failed in TwoFactorManifold\n");
			}

			// Get (xGTG)^(-1) using the previously obtained U
			dpotri_(GLOBAL::U,&r,xGTGinvptr,&r,&info);

			// Check success
			if(info!=0){
				throw std::runtime_error("Cholesky-based inversion failed in TwoFactorManifold\n");
			}

			// Compute (xHTH)^(-1)

			// Construct an object for (xHTH)^(-1) initially holding xHTH since dpotrf_ uses in/out argument
			SharedSpace* xHTHinv = xHTH->ConstructEmpty();
			xHTH->CopyTo(xHTHinv);

			// Use ObtainWritePartialData() to preserve the previous data but allow modification
			double* xHTHinvptr = xHTHinv->ObtainWritePartialData();
			
			// Get the U of Cholesky decomposition of xHTH
			dpotrf_(GLOBAL::U,&r,xHTHinvptr,&r,&info);
			
			// Check success
			if(info!=0){
				throw std::runtime_error("Cholesky decomposition failed in TwoFactorManifold\n");
			}

			// Get (xHTH)^(-1) using the previously obtained U
			dpotri_(GLOBAL::U,&r,xHTHinvptr,&r,&info);

			// Check success
			if(info!=0){
				throw std::runtime_error("Cholesky-based inversion failed in TwoFactorManifold\n");
			}

			// Add them to x
			x->AddToTempData("GTGinv",xGTGinv);
			x->AddToTempData("HTHinv",xHTHinv);
			return;
		}
	}

	void TwoFactorManifold::Retraction(Variable *x, Vector *etax, Variable *result, double instepsize) const{
		// Get pointers
		const double *etaxptr = etax->ObtainReadData();
		const double *xptr = x->ObtainReadData();
		double *resultptr = result->ObtainWriteEntireData();
		integer len=mtimesr+ntimesr;

		// resultM <- xM, details: http://www.netlib.org/lapack/explore-html/da/d6c/dcopy_8f.html
		if (resultptr != xptr){
			// Copy x into result
			dcopy_(&len, xptr, &GLOBAL::IONE, resultptr, &GLOBAL::IONE);
		}
		// resultM <- v + resultTV, details: http://www.netlib.org/lapack/explore-html/d9/dcd/daxpy_8f.html
		daxpy_(&len, &GLOBAL::DONE, etaxptr, &GLOBAL::IONE, resultptr, &GLOBAL::IONE);

	}

}; /*end of ROPTLIB namespace*/