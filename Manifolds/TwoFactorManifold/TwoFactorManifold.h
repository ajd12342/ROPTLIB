/*
This manifold defines the class for the low-rank manifold R_r^{m \times n} represented as
a pair of two Euclidean manifolds G and H, R^{m \times r}, R^{n \times r} obtainable as
G \times H^T .

It is made for the GraphRegLRMatrixCompletion problem by defining a metric,
a vector transport and a projection.

-- Diwan Anuj Jitendra, 2019
*/

#ifndef TWOFACTOR_H
#define TWOFACTOR_H

#include "Manifolds/ProductManifold.h"
#include "Manifolds/Euclidean/Euclidean.h"
#include "Manifolds/TwoFactorManifold/TwoFactorVariable.h"
#include "Manifolds/TwoFactorManifold/TwoFactorVector.h"
#include "Others/MyMatrix.h"
#include <stdexcept>

/*Define the namespace*/
namespace ROPTLIB{
	class TwoFactorManifold: public ProductManifold{
		public:
			/*
			Construct the low rank manifold of m by n matrices with rank r.
			It is represented by (R(m,r), R^(n,r)), i.e.,
			X = G*H^T. G \in R(m,r), H \in R(n,r),
			*/
			TwoFactorManifold(integer inm, integer inn, integer inr, integer inmetric);

			/*
			Delete the manifold
			*/
			~TwoFactorManifold(void);

			/*
			Metric for the TwoFactorManifold.
			metric: 0 = Preconditioned
					Tr((H^T*H)^T*((xi_G)^T*eta_G))+
					Tr((G^T*G)^T*((xi_H)^T*eta_H))

					Computed as depicted by the parantheses.
					Tr(A^T*B) computed as dot(A(:),B(:))

					1 = Left-invariant
			*/
			virtual double Metric(Variable *x, Vector *etax, Vector *xix) const;
			
			/*EucGrad to Grad:
			metric: 0 = Preconditioned
					(dG * (H^T*H)^(-1), dH * (G^T*G)^(-1))
					This is done in the straightforward way i.e.
					compute inverse of H^T*H and left-multiply dG.
					The following idea was trashed because of
					the cost of transposing B (O(m*r)). The Cholesky
					solver is anyways of the same order of complexity
					as Cholesky inverse, though slightly faster.
					IGNORE START:
					// This is computed by using Cholesky since
					// H^T*H is symmetric. So letting dG=B, and
					// (H^T*H)=A, then to get x = B*A^(-1),
					// solve A(x_2)=B^T using dposv since A is SPD,
					// and get x_2 = A^(-1)*B^T. then,
					// x=(x_2)^T = B*A^(-T)=B*A^(-1)
					IGNORE END.

					1= Left-invariant 
					(dG * (G^T*G), dH * (H^T*H))
			*/
			virtual void EucGradToGrad(Variable *x, Vector *egf, Vector *gf, const Problem *prob) const;

			/*
			Computes G^T*G and H^T*H at a point x and stores it in x.
			*/
			virtual void GTGHTH(Variable *x) const;

			/*
			Computes (G^T*G)^(-1) and (H^T*H)^(-1) at a point x and stores it in x.
			Uses the symmetric positive-definiteness of GTG and HTH for this(Cholesky)
			*/
			virtual void GTGinvHTHinv(Variable *x) const;

			/*
			Computes the resultG = etaxG + xG
					     resultH = etaxH + xH
			*/
			virtual void Retraction(Variable *x, Vector *etax, Variable *result, double instepsize) const;

			// /*
			// Projection:
			// */
			// virtual void Projection(Variable *x, Vector *v, Vector *result) const;

			// /*
			// Vector transport:
			// */
			// virtual void VectorTransport(Variable *x, Vector *etax, Variable *y, Vector *xix, Vector *result) const;


		protected:
		integer m; /*the number of row*/
		integer n; /*the number of column*/
		integer r; /*the rank of the matrix*/
		integer mtimesn;
		integer mtimesr;
		integer ntimesr;
		integer rtimesr;
		integer metric; /* Which metric to use? 0 for preconditioned, 1 for left-invariant */
	};
};/*end of ROPTLIB namespace*/
#endif // end of TWOFACTOR_H