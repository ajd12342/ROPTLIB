function [Gfinal,Hfinal,stats,data,Ginit,Hinit,AdjLr,AdjLc]=GRMCTest(compile,m,n,r,nzmaxposs,hyperparams,optimizer,metric)
% hyperparams = [alpha_r, gamma_r, alpha_c, gamma_c]
%% Initial
close all;
addpath BinaryFiles;
% m=100;
% n=80;
% r=10;
% nzmaxposs=4000;
% optimizer='RCG';

%% Produce data
% Randomly permute 1 to m*n
indices=randperm(m*n);

% Effectively get nzmaxposs distinct indices from 1 to m*n
indices=indices(1:nzmaxposs);
[i,j]=ind2sub([m,n],indices);

% Randomly generate G and H
G=randn([m,r]);
H=randn([n,r]);
prod=G*H';

% Produce a 1D-dense array corresponding to non-zero entries of the data
data=prod(indices);

% Produce a sparse matrix from data. nzmaxposs is in case one of the data elements
% is exactly 0, in which case it will not appear in data and nzmax will decrease.
data=sparse(i,j,data,m,n);

% Produce initial iterates perturbed from the solution
Ginit=G+0.2*randn([m,r]);
Hinit=H+0.2*randn([n,r]);

%% Produce graphs
% AdjLr is drawn from std normal
AdjLr=randn([m,m]);

% Made symmetric
AdjLr=AdjLr+AdjLr';

% Made logical. Essentially a Bernoulli(0.5) symmetric matrix
AdjLr=AdjLr>0;
AdjLr=sparse(AdjLr);

% Remove diagonal entries(self loops) if any
AdjLr=AdjLr-diag(diag(AdjLr));

% abs(std normal) generated weights
WtLr=abs(randn([m,m]));

% Made symmetric
WtLr=WtLr+WtLr';

% Weighted adjacency matrix
AdjLr=AdjLr.*WtLr;

% Laplacian
Lr = sparse(diag(sum(AdjLr,2))-AdjLr);

% AdjLc is drawn from std normal
AdjLc=randn([n,n]);

% Made symmetric
AdjLc=AdjLc+AdjLc';

% Made logical. Essentially a Bernoulli(0.5) symmetric matrix
AdjLc=AdjLc>0;
AdjLc=sparse(AdjLc);

% Remove diagonal entries(self loops) if any
AdjLc=AdjLc-diag(diag(AdjLc));

% abs(std normal) generated weights
WtLc=abs(randn([n,n]));

% Made symmetric
WtLc=WtLc+WtLc';

% Weighted adjacency matrix
AdjLc=AdjLc.*WtLc;

% Laplacian
Lc = sparse(diag(sum(AdjLc,2))-AdjLc);
Lc = sparse(n,n);
%% Set hyperparameters
alpha_r=hyperparams(1);
alpha_c=hyperparams(2);
gamma_r=hyperparams(3);
gamma_c=hyperparams(4);
Lreg_Lr = alpha_r*speye(m) + alpha_r*gamma_r*sparse(Lr);
Lreg_Lc = alpha_c*speye(n) + alpha_c*gamma_c*sparse(Lc);
%% Execute
if(compile==1)
MyMexGRMC;
end
[Gfinal, Hfinal, stats] = TestGraphRegLRMatrixCompletion(...
    data,data, r,Lreg_Lr,Lreg_Lc,...
    alpha_r,alpha_c,gamma_r,gamma_c,Ginit,Hinit,optimizer,metric);
Gfinal = Gfinal';
Hfinal = Hfinal';
stats = stats';
%% Compute cost using MATLAB
end