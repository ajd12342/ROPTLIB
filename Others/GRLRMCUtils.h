/*
Utilities to make it easier to use ROPTLIB

-- Anuj Jitendra Diwan, 2019
*/

#include <utility>
#include "Others/def.h"
// #include "Others/SparseBLAS/blas_sparse.h"

// blas_sparse_matrix makeSparse(double* matrix, long nz, long m, long n, long* i, long* j);

/*
Multiplies a sparse matrix S of size m*n with 
a dense matrix X of size n*r when transS = false
a dense matrix X of size m*r when transS = true

result = S * X when transS = false
result = S^T * X when transS = true

Arguments:
double* S (in)- The sparse matrix as an long array of size nS, the number of non-zero entries in S
long* I (in)- The row coordinates of the non-zero entries of S, in the same order as the double* S input
long* J (in)- The column coordinates of the non-zero entries of S, in the same order as the double* S input
long nS (in)- The number of non-zero entries in S
bool transS (in)- false = No transpose, true = Transpose
X (in)- The dense matrix as a 1-D array of size n*r(transS = false) or size m*r(transS = true) in column-major order.
m (in)- The first dimension of S
n (in)- The second dimension of S
r (in)- The second dimension of X
result (out)- The result of the computation will be written into result. 
Ensure that result is allocated m*r space(transS = false) or n*r space(transS = true)

Complexity: O(2*k1*r+max(m,n)*r)

Adapted from com2_egradf by Shuyu Dong, UCLouvain (shuyu.dong@uclouvain.be).
*/
void SpDenseMatMult(const double* S, const integer* I, const integer* J, const integer nS, const bool transS,
					const double* X, const integer m, const integer n, const integer r, double* result);