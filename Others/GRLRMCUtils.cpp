#include "Others/GRLRMCUtils.h"

// blas_sparse_matrix makeSparse(double* matrix, long nz, long m, long n, long* i, long* j){
// 	int* iint = new int[nz];
// 	int* jint = new int[nz];
// 	for(long it=0;it<nz;it++){
// 		iint[it]=i[it];
// 		jint[it]=j[it];
// 	}
// 	blas_sparse_matrix ret;
// 	ret=BLAS_duscr_begin(m,n);
// 	BLAS_duscr_insert_entries(ret,nz,matrix,iint,jint);
// 	BLAS_duscr_end(ret);
// 	delete[] iint;
// 	delete[] jint;
// 	return ret;
// }

void SpDenseMatMult(const double* S, const integer* I, const integer* J, const integer nS, const bool transS,
					const double* X, const integer m, const integer n, const integer r, double* result){

	// Set the size of result
	integer resultSize=0;
	if(transS){
		resultSize=n*r;
	}else{
		resultSize=m*r;
	}

	// Initialize result to all zeros
	for(integer i=0;i<resultSize;i++){
		result[i]=0;
	}

	// Compute the product

	// Iterate over each element of S
	for(integer it=0; it<nS; it++){
		/* Compute the increment 
         * DG(i_l,k)=sum_{j_l\in\Omega_{i_l}} S(i_l,j_l)*H(j_l,k), for each
         * k in [1,r]. 
         */

		// Iterate over the row of the result to which this element can contribute
        for (integer k=0; k<r; ++k){
        	if(transS){
        		result[J[it] + n*k] += S[it]*X[I[it] + m*k];
	        }else{
				result[I[it] + m*k] += S[it]*X[J[it] + n*k];
	        }
        }
	}
}