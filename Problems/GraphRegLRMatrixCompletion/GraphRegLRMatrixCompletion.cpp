
#include "Problems/GraphRegLRMatrixCompletion/GraphRegLRMatrixCompletion.h"

/*Define the namespace*/
namespace ROPTLIB{

	GraphRegLRMatrixCompletion::GraphRegLRMatrixCompletion(double *indata, integer innz, integer *inir, integer *injc,
			double *indataVal, integer innzVal, integer *inirVal, integer *injcVal,
			integer inm, integer inn, integer inr,
			double *inLr, integer innr, integer* iniLr, integer* injLr,
			double *inLc, integer innc, integer* iniLc, integer* injLc, 
			double inalpha_r, double inalpha_c, double ingamma_r, double ingamma_c){
		data = indata;
		nz = innz;
		ir = inir;
		jc = injc;

		dataVal = indataVal;
		nzVal = innzVal;
		irVal = inirVal;
		jcVal = injcVal;

		m = inm;
		n = inn;
		r = inr;
		mtimesn=m*n;
		mtimesr=m*r;
		ntimesr=n*r;

		Lrptr = inLr;
		nr = innr;
		iLr = iniLr;
		jLr = injLr;
		// Lr = makeSparse(Lrptr,nr,m,m,iLr,jLr); // Unused

		Lcptr = inLc;
		nc = innc;
		iLc = iniLc;
		jLc = injLc;
		// Lc = makeSparse(Lcptr,nc,n,n,iLc,jLc); // Unused

		alpha_r = inalpha_r;
		alpha_c = inalpha_c;
		gamma_r = ingamma_r;
		gamma_c = ingamma_c;
		alpha_rtimesgamma_r=alpha_r*gamma_r;
		alpha_ctimesgamma_c=alpha_c*gamma_c;

		// Since we use only gradient-based optimization schemes
		SetUseGrad(true);
		SetUseHess(false);
	}

	GraphRegLRMatrixCompletion::~GraphRegLRMatrixCompletion(void){
		//  /* Deletes the blas_sparse_matrices */ 
		// BLAS_usds(Lr);
		// BLAS_usds(Lc);
	}

	/*
	Input: x, containing the current G, H as a TwoFactorVariable(ProductElement)
	Output: The value of f, the cost function at x
	*/
	double GraphRegLRMatrixCompletion::f(Variable *x) const{
		return
		GraphRegLRMatrixCompletion::diffF(x)
		+GraphRegLRMatrixCompletion::regul_precreated(x);
	}
	/*
	Input: 
	x, containing the current G,H as a TwoFactorVariable(ProductElement)
	*/
	double GraphRegLRMatrixCompletion::regul(Variable *x) const{
		// No checks are performed on sizes
		TwoFactorVariable *twox = dynamic_cast<TwoFactorVariable *>(x);
		const double *Gptr = twox->GetElement(0)->ObtainReadData();
		const double *Hptr = twox->GetElement(1)->ObtainReadData();
		
		double result=0.0;
		double resultG=0.0;
		double resultH=0.0;

		//LG,LH precomputation
		GraphRegLRMatrixCompletion::LGandLH(x);
		const double *LGptr=x->ObtainReadTempData("LG")->ObtainReadData();
		const double *LHptr=x->ObtainReadTempData("LH")->ObtainReadData();

		//For G
		//Frob norm
		resultG+=(alpha_r/2.0)*ddot_(&mtimesr,Gptr,&GLOBAL::IONE,Gptr,&GLOBAL::IONE);

		//Now, we compute full dot product of G and LG (exactly trace of G^TLG)
		resultG+=(alpha_r/2.0)*gamma_r*ddot_(&mtimesr,Gptr,&GLOBAL::IONE,LGptr,&GLOBAL::IONE);

		//We are done with G!

		//For H
		//Frob norm
		resultH+=(alpha_c/2.0)*ddot_(&ntimesr,Hptr,&GLOBAL::IONE,Hptr,&GLOBAL::IONE);

		//Now, we compute full dot product of H and LH (exactly trace of H^TLH)
		resultH+=(alpha_c/2.0)*gamma_c*ddot_(&ntimesr,Hptr,&GLOBAL::IONE,LHptr,&GLOBAL::IONE);

		//We are done with H!

		result+=(resultG+resultH);
		return result;
	}
	/*
	Input: 
	x, containing the current G,H as a TwoFactorVariable(ProductElement)
	*/
	double GraphRegLRMatrixCompletion::regul_precreated(Variable *x) const{
		// No checks are performed on sizes
		TwoFactorVariable *twox = dynamic_cast<TwoFactorVariable *>(x);
		const double *Gptr = twox->GetElement(0)->ObtainReadData();
		const double *Hptr = twox->GetElement(1)->ObtainReadData();
		
		double result=0.0;
		double resultG=0.0;
		double resultH=0.0;

		//LG,LH precomputation
		GraphRegLRMatrixCompletion::LGandLH(x);
		const double *LGptr=x->ObtainReadTempData("LG")->ObtainReadData();
		const double *LHptr=x->ObtainReadTempData("LH")->ObtainReadData();

		//For G
		//Now, we compute full dot product of G and LG (exactly trace of G^TLG)
		resultG+=0.5*ddot_(&mtimesr,Gptr,&GLOBAL::IONE,LGptr,&GLOBAL::IONE);

		//We are done with G!

		//For H
		//Now, we compute full dot product of H and LH (exactly trace of H^TLH)
		resultH+=0.5*ddot_(&ntimesr,Hptr,&GLOBAL::IONE,LHptr,&GLOBAL::IONE);

		//We are done with H!

		result+=(resultG+resultH);
		return result;
	}

	double GraphRegLRMatrixCompletion::diffF(Variable *x) const{
		//Precompute S
		GraphRegLRMatrixCompletion::S(x,0);
		const double* Sptr = x->ObtainReadTempData("S")->ObtainReadData();

		double result=ddot_(&nz,Sptr,&GLOBAL::IONE,Sptr,&GLOBAL::IONE);
		result*=0.5;
		return result;
	}

	void GraphRegLRMatrixCompletion::LGandLH(Variable *x) const{
		if(x->TempDataExist("LG") && x->TempDataExist("LH")){
			return;
		}else{
			TwoFactorVariable *twox = dynamic_cast<TwoFactorVariable *>(x);
			const double *Gptr = twox->GetElement(0)->ObtainReadData();
			const double *Hptr = twox->GetElement(1)->ObtainReadData();

			//LG
			SharedSpace *LG = new SharedSpace(2,m,r);
			double *LGptr = LG->ObtainWriteEntireData();
			// BLAS_dusmm(blas_colmajor,blas_no_trans,r,1.0,Lr,Gptr,m,LGptr,m);
			SpDenseMatMult(Lrptr,iLr,jLr,nr,false,Gptr,m,m,r,LGptr);
			x->AddToTempData("LG",LG);

			//LH
			SharedSpace *LH = new SharedSpace(2,n,r);
			double *LHptr = LH->ObtainWriteEntireData();
			// BLAS_dusmm(blas_colmajor,blas_no_trans,r,1.0,Lc,Hptr,n,LHptr,n);	
			SpDenseMatMult(Lcptr,iLc,jLc,nc,false,Hptr,n,n,r,LHptr);
			x->AddToTempData("LH",LH);
		}
	}

	void GraphRegLRMatrixCompletion::S(Variable *x, int type) const{
		// *S named variables to point to either train or test variables depending on type
		integer* irS = nullptr;
		integer* jcS = nullptr;
		double* dataS = nullptr;
		integer nzS = 0;
		if(type == 0){
			if(x->TempDataExist("S")){
				return;
			}
			irS = ir;
			jcS = jc;
			dataS = data;
			nzS = nz;
		}else if(type == 1){
			if(x->TempDataExist("S_test")){
				return;
			}
			irS = irVal;
			jcS = jcVal;
			dataS = dataVal;
			nzS = nzVal;
		}else{
			return;
		}
		TwoFactorVariable *twox = dynamic_cast<TwoFactorVariable *>(x);
		const double *Gptr = twox->GetElement(0)->ObtainReadData();
		const double *Hptr = twox->GetElement(1)->ObtainReadData();
		// First, compute GH^T only the indices in irS and jcS
		SharedSpace *GHT = new SharedSpace(1,nzS);
		double *GHTptr = GHT->ObtainWriteEntireData();
		// No checks are performed on the sizes of G,H,irS,jcS, etc.
		for(integer iter=0;iter<nzS;iter++){
			integer i=irS[iter];
			integer j=jcS[iter];

			// Uses BLAS here by pulling out G(i,:) and H(j,:)
			GHTptr[iter]=ddot_(&r,Gptr+i,&m,Hptr+j,&n);
		}
		/*
		Now, subtract dataS from this.
		*/
		daxpy_(&nzS,&GLOBAL::DNONE,dataS,&GLOBAL::IONE,GHTptr,&GLOBAL::IONE);

		// /* Store the blas_sparse_matrix(a.k.a. int) as a double in a SharedSpace */
		// SharedSpace *SSparse = new SharedSpace(1,1);
		// double* SSparseptr = SSparse->ObtainWriteEntireData();

		// blas_sparse_matrix SSparseMat=makeSparse(GHTptr,nzS,m,n,irS,jcS);
		// *SSparseptr=SSparseMat; //Automatic typecasting
		if(type == 0){
			x->AddToTempData("S",GHT);
		}else if(type == 1){
			x->AddToTempData("S_test",GHT);
		}
		// x->AddToTempData("SSparse",SSparse);
	}

	void GraphRegLRMatrixCompletion::EucGrad_notprecreated(Variable *x, Vector *egf) const{
		TwoFactorVariable *twox = dynamic_cast<TwoFactorVariable *>(x);
		const double *Gptr = twox->GetElement(0)->ObtainReadData();
		const double *Hptr = twox->GetElement(1)->ObtainReadData();

		TwoFactorVector *twoegf = dynamic_cast<TwoFactorVector *>(egf);
		twoegf->NewMemoryOnWrite();
		double *dGptr = twoegf->GetElement(0)->ObtainWriteEntireData();
		double *dHptr = twoegf->GetElement(1)->ObtainWriteEntireData();
		
		// for(int i=0;i<twoegf->Getls();i++){
		// 	std::cout<<"Dim "<<i<<": "<<twoegf->Getsize()[i]<<std::endl;
		// }		
		
		//Precompute S
		GraphRegLRMatrixCompletion::S(x,0);
		// const blas_sparse_matrix S=round(*(x->ObtainReadTempData("SSparse")->ObtainReadData()));
		const double* Sptr = x->ObtainReadTempData("S")->ObtainReadData();

		//Precompute LG and LH
		GraphRegLRMatrixCompletion::LGandLH(x);
		const double *LGptr=x->ObtainReadTempData("LG")->ObtainReadData();
		const double *LHptr=x->ObtainReadTempData("LH")->ObtainReadData();

		// Compute dG

		// SH
		// BLAS_dusmm(blas_colmajor,blas_no_trans,r,1.0,S,Hptr,n,dGptr,m);

		// Compute using one's own function
		SpDenseMatMult(Sptr,ir,jc,nz,false,Hptr,m,n,r,dGptr);

		// SH + alpha_r*G
		daxpy_(&mtimesr,&alpha_r,Gptr,&GLOBAL::IONE,dGptr,&GLOBAL::IONE);

		// SH + alpha_r*G + alpha_r*gamma_r*L_r*G
		daxpy_(&mtimesr,&alpha_rtimesgamma_r,LGptr,&GLOBAL::IONE,dGptr,&GLOBAL::IONE);

		
		//Compute dH

		// S^TG
		// BLAS_dusmm(blas_colmajor,blas_trans,r,1.0,S,Gptr,m,dHptr,n);

		// Compute using one's own function
		SpDenseMatMult(Sptr,ir,jc,nz,true,Gptr,m,n,r,dHptr);

		// S^TG + alpha_c*H
		daxpy_(&ntimesr,&alpha_c,Hptr,&GLOBAL::IONE,dHptr,&GLOBAL::IONE);

		// S^TG + alpha_c*H + alpha_c*gamma_c*L_c*H
		daxpy_(&ntimesr,&alpha_ctimesgamma_c,LHptr,&GLOBAL::IONE,dHptr,&GLOBAL::IONE);

	}

	void GraphRegLRMatrixCompletion::EucGrad(Variable *x, Vector *egf) const{
		TwoFactorVariable *twox = dynamic_cast<TwoFactorVariable *>(x);
		const double *Gptr = twox->GetElement(0)->ObtainReadData();
		const double *Hptr = twox->GetElement(1)->ObtainReadData();

		TwoFactorVector *twoegf = dynamic_cast<TwoFactorVector *>(egf);
		twoegf->NewMemoryOnWrite();
		double *dGptr = twoegf->GetElement(0)->ObtainWriteEntireData();
		double *dHptr = twoegf->GetElement(1)->ObtainWriteEntireData();
		
		// for(int i=0;i<twoegf->Getls();i++){
		// 	std::cout<<"Dim "<<i<<": "<<twoegf->Getsize()[i]<<std::endl;
		// }		
		
		//Precompute S
		GraphRegLRMatrixCompletion::S(x,0);
		// const blas_sparse_matrix S=round(*(x->ObtainReadTempData("SSparse")->ObtainReadData()));
		const double* Sptr = x->ObtainReadTempData("S")->ObtainReadData();

		//Precompute LG and LH
		GraphRegLRMatrixCompletion::LGandLH(x);
		const double *LGptr=x->ObtainReadTempData("LG")->ObtainReadData();
		const double *LHptr=x->ObtainReadTempData("LH")->ObtainReadData();

		// Compute dG

		// SH
		// BLAS_dusmm(blas_colmajor,blas_no_trans,r,1.0,S,Hptr,n,dGptr,m);

		// Compute using one's own function
		SpDenseMatMult(Sptr,ir,jc,nz,false,Hptr,m,n,r,dGptr);

		// SH + L_r*G
		daxpy_(&mtimesr,&GLOBAL::DONE,LGptr,&GLOBAL::IONE,dGptr,&GLOBAL::IONE);

		
		//Compute dH

		// S^TG
		// BLAS_dusmm(blas_colmajor,blas_trans,r,1.0,S,Gptr,m,dHptr,n);

		// Compute using one's own function
		SpDenseMatMult(Sptr,ir,jc,nz,true,Gptr,m,n,r,dHptr);

		// S^TG + L_c*H
		daxpy_(&ntimesr,&GLOBAL::DONE,LHptr,&GLOBAL::IONE,dHptr,&GLOBAL::IONE);

	}

	void GraphRegLRMatrixCompletion::EucHessianEta(Variable *x, Vector *etax, Vector *exix) const{
		etax->CopyTo(exix);
	}

	double GraphRegLRMatrixCompletion::RMSE(Variable *x, int type) const{
		integer nzS = 0;
		std::string S_string; 
		if(type == 0){
			nzS = nz;
			S_string = "S";
		}else if(type == 1){
			nzS = nzVal;
			S_string = "S_test";
		}else{
			return -1;
		}

		GraphRegLRMatrixCompletion::S(x,type);
		const double* Sptr = x->ObtainReadTempData(S_string)->ObtainReadData();

		double result=ddot_(&nz,Sptr,&GLOBAL::IONE,Sptr,&GLOBAL::IONE)/nzS;
		result=sqrt(result);
		return result;
	}
}; /*end of ROPTLIB namespace*/
