/*
This file defines the class for the problem
min_{G \in R^{m by r},H \in R^{n by r}} 0.5 ||P_Ω(X) - P_Ω(G*H^T)||_F^2 + R_L[r](G) + R_L[c](H), 
where Ω is a index set, A_{ij}, (i, j) \in Ω are given,
where R_L[x](Q) = (alpha_x/2)*(||Q||_F^2 + gamma_x*Tr(Q^T*L_x*Q))
for subscript indexer x(here, r and c) and matrix Q.

Problem --> GraphRegLRMatrixCompletion

---- Diwan Anuj Jitendra, 2019
*/

#ifndef GRAPHREGLRMATRIXCOMPLETION_H
#define GRAPHREGLRMATRIXCOMPLETION_H

#include "Problems/Problem.h"
#include "Manifolds/SharedSpace.h"
// #include "Others/SparseBLAS/blas_sparse.h"
#include "Others/def.h"
#include "Others/GRLRMCUtils.h"
#include "Manifolds/Element.h"
#include "Manifolds/ProductElement.h"
#include "Manifolds/ProductManifold.h"
#include "Manifolds/TwoFactorManifold/TwoFactorManifold.h"
#include "Manifolds/TwoFactorManifold/TwoFactorVariable.h"
#include "Manifolds/TwoFactorManifold/TwoFactorVector.h"

#include <stdexcept>
// #include "Manifolds/LowRank/LowRank.h"

/*Define the namespace*/
namespace ROPTLIB{

	class GraphRegLRMatrixCompletion : public Problem{
	public:
		GraphRegLRMatrixCompletion(double *indata, integer innz, integer *inir, integer *injc,
			double *indataVal, integer innzVal, integer *inirVal, integer *injcVal,
			integer inm, integer inn, integer inr,
			double *inLr, integer innr, integer* iniLr, integer* injLr,
			double *inLc, integer innc, integer* iniLc, integer* injLc, 
			double inalpha_r, double inalpha_c, double ingamma_r, double ingamma_c);

		virtual ~GraphRegLRMatrixCompletion();

		/* 0.5 ||P_Ω(X) - P_Ω(G*H^T)||_F^2 + R_L[r](G) + R_L[c](H) */
		virtual double f(Variable *x) const;

		/* R_L[x][Q] = (alpha_x/2)*(||Q||_F^2 + gamma_x*Tr(Q^T*L_x*Q)). */
		virtual double regul(Variable *x) const;

		/* R_L[x][Q] = Tr(Q^T*L_x*Q). */
		virtual double regul_precreated(Variable *x) const;

		/* 0.5 ||P_Ω(X) - P_Ω(G*H^T)||_F^2 and stores GHT and Subtract in x*/
		virtual double diffF(Variable *x) const;

		/* Computes P_Ω_train(G*H^T - X_train) if type=0 and P_Ω_test(G*H^T - X_test) if type=1 and stores it as a long dense vector in x */
		virtual void S(Variable *x, int type) const;

		/* Computes L_r*G and L_c*H and stores it in x */
		virtual void LGandLH(Variable *x) const;

		/* (S*H + alpha_r*G + alpha_r*gamma_r*L_r*G,
		 	S^T*G + alpha_c*H + alpha_c*gamma_c*L_c*H) */
		virtual void EucGrad_notprecreated(Variable *x, Vector *egf) const;

		/* (S*H + L_r*G,
		 	S^T*G + L_c*H) */
		virtual void EucGrad(Variable *x, Vector *egf) const;

		/* Dummy function, just copies etax to exix. Required for CheckGradHessian */
		virtual void EucHessianEta(Variable *x, Vector *etax, Vector *exix) const;

		/* Computes sqrt(||P_Ω(X) - P_Ω(G*H^T)||_F^2/|Ω|) for either train(type=0) or test(type=1) */
		virtual double RMSE(Variable *x, int type) const;

		integer *ir;
		integer *jc;
		double *data;
		integer nz;

		integer *irVal;
		integer *jcVal;
		double *dataVal;
		integer nzVal;

		integer m;
		integer n;
		integer r;
		integer mtimesn;
		integer mtimesr;
		integer ntimesr;

		double *Lrptr;
		integer nr;
		integer* iLr;
		integer* jLr;
		// blas_sparse_matrix Lr;

		double *Lcptr;
		integer nc;
		integer* iLc;
		integer* jLc;
		// blas_sparse_matrix Lc;

		double alpha_r;
		double alpha_c;
		double gamma_r;
		double gamma_c;
		double alpha_rtimesgamma_r;
		double alpha_ctimesgamma_c;
	};
}; /*end of ROPTLIB namespace*/
#endif
